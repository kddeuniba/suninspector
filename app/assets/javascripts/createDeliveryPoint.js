require([ "jquery" ], function($) {
	
	var mapsProvider = 'googlev3';

	var map = new mxn.Mapstraction('map',mapsProvider);
	map.addMapTypeControls();
	map.addLargeControls();
	var latlon = new mxn.LatLonPoint(41.12, 16.87);
	map.setCenterAndZoom(latlon, 10);
		
	var geocoder = new mxn.Geocoder(mapsProvider,geocode_return);
	
	
	function geocode_return(geocoded_location) {
		
		map.removeAllMarkers();
		
		// display the map centered on a latitude and longitude (Google zoom levels)
		map.setCenterAndZoom(geocoded_location.point, 15);

		// create a marker positioned at a lat/lon
		var geocode_marker = new mxn.Marker(geocoded_location.point);

		var address = geocoded_location.street + ", " + geocoded_location.postcode + ", " + 
			geocoded_location.locality + ", " + geocoded_location.region + ", " + geocoded_location.country;
		geocode_marker.setInfoBubble(address);

		// display marker
		map.addMarker(geocode_marker);
		map.setZoom(10);
		// open the marker
		//geocode_marker.openBubble();
		
		//set the point
		var point = "(" + geocoded_location.point.toString() + ")";
		$("#position").val(point);
		//update the text
		$("#street").val(address);
	}
	
	//add click for address find
	$("#searchAddress").click(function(e) {
		e.preventDefault();
		//get the address
		var sAddress = $("#street").val();

		if (sAddress.length == 0)
			return false;
		
		geocoder.geocode(sAddress);
		
	});
});

