var selectedId = -1;
var x, y,w;

require([ "datatables/jquery.dataTables" ], function(dt) {
		
	jsRoutes.controllers.DeliveryPoint.list().ajax({
	    success: function(data) {
	    	loadDataInTable(data);
	      },
	      error: function() {
	        alert("Error!")
	      }
	    });
	
	
	function loadDataInTable(data) {
		
		var oTable = $('#dpTable').dataTable({
			 "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
			 "bPaginate": true,
		     "bLengthChange": true,
		     "bFilter": true,
		     "bSort": true,
		     "bInfo": true,
		     "bAutoWidth": true,
		     "bDestroy": true,
		     "aaData": data,
		     "aoColumns": [
		        { "mDataProp": "idString" },
		        { "mDataProp": "street" },
		        { "mDataProp": "pvPlant.peakPower" },
		        { "mDataProp": "pvPlant.area" },
		        { "mDataProp": "pvPlant.type" },
		        { "mDataProp": "pvPlant.place" },
		        { "mDataProp": "pvPlant.technology" },
		        { "mDataProp": "status" },
		        { "mDataProp": "owner" }
		    ]
		});	
		
		
		$("#dpTable tbody").click(function(event) {
			$(oTable.fnSettings().aoData).each(function (){
				$(this.nTr).removeClass('row_selected');
			});
			$(event.target.parentNode).addClass('row_selected');
			selectedId = $(event.target.parentNode.childNodes[0]).text();
			loadDatesSelect(selectedId);
		});
	}
	
	function loadDatesSelect(id){
	    
	    jsRoutes.controllers.Production.distinctDates(id).ajax({
		
		success: function(data) {
		    $('#selectday').empty();
		    $('#selectday').append($('<option>', {
			      value: -1,
			      text: "select a day"
			  }));
		    
		    $.each(data, function(i,item){
			  $('#selectday').append( $('<option>', {
			      value: item.date,
			      text: new Date(item.date)
			  }));
		    });	
		    
		    
		    $('#selectday option:first-child').attr("selected", "selected");
		    		    
		    $('#selectday').change(function(e) {
			var optionSelected = $("option:selected", this);
			var valueSelected = this.value;
			loadReportProduction(id,valueSelected);
		    });
		},
		error: function() {
		    alert("Error!")
		}
	    });
	}
	
	function loadReportProduction(id,timestamp){
		
		jsRoutes.controllers.Production.listDay(id,timestamp).ajax({
		    success: function(data) {
		    	loadReportInChart(data);
		      },
		      error: function() {
		        alert("Error!")
		      }
		    });
	}
	
	

	    function loadReportInChart(data) {
		
		$('#chart').empty();
		
		if (data.length == 0){
			$('#chart').append('<p> No Data </p>');
		    return;
		}
		
		var margin = {top: 20, right: 20, bottom: 130, left: 40},
		    width = 860 - margin.left - margin.right,
		    height = 500 - margin.top - margin.bottom;
		
		//prepare x and y axis
		var x = d3.scale.ordinal().rangeRoundBands([0,width], .2);
		var y = d3.scale.linear().range([height,0]);
		var xAxis = d3.svg.axis().scale(x).orient("bottom");
		var yAxis = d3.svg.axis().scale(y).orient("left");
		
		x.domain(data.map(function(d) { return d.timestamp; }));
		y.domain([0, d3.max(data, function(d) { return d.energy; })]);
		
		//select the node
		var chart = d3.select("#chart").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
		//append the generated x axis
		chart.append("g")
		.attr("class", "x axis")
		      .attr("transform", "translate(0," + height + ")")
		      .call(xAxis)
		      .selectAll("text")  
                    .style("text-anchor", "end")
                    .attr("dx", "-.8em")
                    .attr("dy", ".15em")
                    .attr("transform", function(d) {
                        return "rotate(-65)" 
                            });
		
		chart.append("g")
		      .attr("class", "y axis")
		      .call(yAxis)
		    .append("text")
		      .attr("transform", "rotate(-90)")
		      .attr("y", 6)
		      .attr("dy", ".71em")
		      .style("text-anchor", "end")
		      .text("Energy Produced KW/h");
		
		chart.selectAll(".bar").data(data).enter().append("rect")
			.attr("class", "bar")
			.attr("x", function(d) {return x(d.timestamp);})
			.attr("y", function(d) {return y(d.energy);})
			.attr("width", x.rangeBand())
			.attr("height", function(d) { return height - y(d.energy); });
		
		
	    }
});
