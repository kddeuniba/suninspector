var dataClusters;

$(document).ready(function(){
        
	// create a map in the "map" div, set the view to a given place and zoom
    var map = L.map('trendMap').setView([41.505, 16.08], 8);

    // add an OpenStreetMap tile layer
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    
    
    $('#selectday').change(function(e) {
	var optionSelected = $("option:selected", this);
	var valueSelected = this.value;
	//alert(valueSelected);
	jsRoutes.controllers.Sumatra.getTrendClustersDate(valueSelected).ajax({
	    success: function(data) {
		//alert(data[0].id);
		dataClusters = data
		loadSpinner(data);
	      },
	      error: function() {
	        alert("Error!")
	      }
	    });
    });
    
    function loadSpinner(data){
	    $.each(data, function(i,item){
		  $('#selectId').append( $('<option>', {
		      value: i,
		      text: item.id
		  }));
	  });	
	}
    
    $('#selectId').change(function(e) {
	var optionSelected = $("option:selected", this);
	var valueSelected = this.value;
	clearMap();
	loadCluster(dataClusters[valueSelected]);
	loadPolyline(dataClusters[valueSelected]);
    });
        
    function loadPolyline(cluster) {
	 var associative = cluster.charts;
	 	 
	 nv.addGraph(function() {
	     var chart = nv.models.cumulativeLineChart()
	                   .x(function(d) { return d[0] })
	                   .y(function(d) { return d[1]}) //adjusting, 100% is 1.00, not 100 as it is in the data
	                   .color(d3.scale.category10().range());

	     chart.xAxis
	         .tickFormat(function(d) {
	           return d3.time.format('%Y-%m-%d %H:%M:%S')(new Date(d))
	         });

	     chart.yAxis
	         .tickFormat(d3.format(',.5f'));

	     d3.select('#chart1 svg')
	         .datum(associative)
	       .transition().duration(500)
	         .call(chart);

	     nv.utils.windowResize(chart.update);

	     return chart;
	   });
	}
    
    function loadCluster(cluster){
	    var pvPlants = cluster.clusters;
	    $.each(pvPlants, function(i,pvPlant){
		 putMarker(i, pvPlant, pvPlant.number);
	    });	
	}
    
    function putMarker(i, pvPlant, number){
	//changed to align with subdomain
	var urlIcon = '/suninspector/assets/images/markers/number_' + number + '.png';
	
	var icon = L.icon({
	    iconUrl: urlIcon,
	    shadowUrl: ''

	  //  iconSize:     [38, 95], // size of the icon
	   // shadowSize:   [50, 64], // size of the shadow
	    //iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
	   // shadowAnchor: [4, 62],  // the same for the shadow
	    //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
	});
	var position = pvPlant.position;
	position = position.replace("(", "");
	position = position.replace(")", "");
	var array = position.split(",");
	var message = "pvPlant with id " + pvPlant.id + ", address " + pvPlant.street
	L.marker([array[0],array[1]], {icon: icon}).addTo(map).bindPopup(message);
    }
    
    function clearMap() {
	    for(i in map._layers) {
	        if(map._layers[i]._path != undefined) {
	            try {
	                m.removeLayer(map._layers[i]);
	            }
	            catch(e) {
	                console.log("problem with " + e + map._layers[i]);
	            }
	        }
	    }
	}
    
});




