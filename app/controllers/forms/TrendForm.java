package controllers.forms;

import java.util.Date;

public class TrendForm {

	public String network;

	public Date startingTime;

	public int intervalMinute = 15;

	public int windowSize = 6;

	public float threshold = 0.8f;
	
	public double maxDistanceEdge = 200d;
	
	public TrendForm() {
	}
}
