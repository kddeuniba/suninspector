package controllers.forms;

import play.data.validation.Constraints.Required;

public class LoginUser {

	@Required
	public String username;
	@Required
	public String password;
}
