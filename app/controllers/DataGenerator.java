package controllers;


import java.util.Date;

import models.PVPlant;
import models.Production;

import org.joda.time.DateTime;

import play.*;
import play.libs.F.Function;
import play.libs.WS;
import play.mvc.*;

import views.html.*;

public class DataGenerator extends Controller {

	public static Result index() {
		return ok(views.html.dataGenerator.index.render(session()));
	}

	public static Result start(Long id) {

		// get the delivery point
		PVPlant pv = PVPlant.findById(id);
		String url = Configuration.root().asMap().get("datageneratorUrl")
				.toString();
		url += pv.asGetUrl() + "start";

		// make the call
		return async(WS.url(url).get().map(new Function<WS.Response, Result>() {
			public Result apply(WS.Response response) {

				String value = response.getBody().toString();
				String split[] = value.split(" ");
				long id = Long.parseLong(split[0]);
				models.enums.Status status = models.enums.Status
						.valueOf(split[1]);

				PVPlant pv = PVPlant.findById(id);
				pv.deliveryPoint.status = status;
				pv.deliveryPoint.save();
				flash("message", "PV Plant " + response.getBody().toString());
				return redirect(controllers.routes.DataGenerator.index());
			}
		}));
	}

	public static Result stop(Long id) {

		// get the delivery point
		PVPlant pv = PVPlant.findById(id);
		String url = Configuration.root().asMap().get("datageneratorUrl")
				.toString();
		url +=  pv.id + "/stop";

		// make the call
		return async(WS.url(url).get().map(new Function<WS.Response, Result>() {
			public Result apply(WS.Response response) {

				String value = response.getBody().toString();
				String split[] = value.split(" ");
				long id = Long.parseLong(split[0]);
				models.enums.Status status = models.enums.Status
						.valueOf(split[1]);

				PVPlant pv = PVPlant.findById(id);
				pv.deliveryPoint.status = status;
				pv.deliveryPoint.save();
				flash("message", "Photovoltaic Plant " + response.getBody().toString());
				return redirect(controllers.routes.DataGenerator.index());
			}
		}));
	}

	public static Result reset(Long id) {
		// get the delivery point
		PVPlant pv = PVPlant.findById(id);
		String url = Configuration.root().asMap().get("datageneratorUrl")
				.toString();
		url +=  pv.id + "/reset";

		// make the call
		return async(WS.url(url).get().map(new Function<WS.Response, Result>() {
			public Result apply(WS.Response response) {

				String value = response.getBody().toString();
				String split[] = value.split(" ");
				long id = Long.parseLong(split[0]);
				models.enums.Status status = models.enums.Status
						.valueOf("stopped");

				PVPlant pv = PVPlant.findById(id);
				pv.deliveryPoint.status = status;
				pv.deliveryPoint.save();
				flash("message", "PV Plant " + response.getBody().toString());
				return redirect(controllers.routes.DataGenerator.index());
			}
		}));
	}

	public static Result receive(Long id, Long timestamp, Double measure,
			Float temperature) {
		String value = id + " " + new DateTime(timestamp) + " " + measure + " "
				+ temperature;
		if (Production.findByPvIdAndTimestamp(id,new Date(timestamp)).isEmpty()){
			Production production = new Production(id, timestamp, measure, temperature);
			production.save();
			//Logger.info(value);
		}
		return ok(value);
	}

}
