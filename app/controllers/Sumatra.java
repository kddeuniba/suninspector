
package controllers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.codehaus.jackson.JsonNode;

import models.TrendCluster;

import clientModels.ChartSeries;
import clientModels.DateDTO;
import clientModels.DeliveryPointDTO;
import clientModels.GroupClusterDTO;
import clientModels.SumatraDTO;
import clientModels.TrendClusterDTO;

import com.google.common.collect.Lists;

import controllers.forms.TrendForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import sumatra.SumatraService;

public class Sumatra extends Controller {
	
	private static Map<Long, SumatraService> mapSumatra = new HashMap<Long, SumatraService>();
	
	private static int delay = 20;
	
	private static List<String> networks = Lists.newArrayList("energy","temperature");
	
	private static final Form<TrendForm> trendForm = new Form<TrendForm>(TrendForm.class);

	public static Result index() {
		TrendForm form = new TrendForm();
		Form<TrendForm> myForm  = trendForm.fill(form);
		return ok(views.html.sumatra.index.render(myForm,networks,session()));
	}
	
	public static Result schedule(){
		Form<TrendForm> myForm = trendForm.bindFromRequest();
		if (myForm.hasErrors()){
			return badRequest(views.html.sumatra.index.render(myForm,networks,session()));
		}
		TrendForm trendForm = myForm.get();
		String sTime = myForm.field("startingTime").value() + ":00";
		trendForm.startingTime = java.sql.Timestamp.valueOf(sTime);
		
		SumatraService service = new SumatraService(delay,trendForm);
		mapSumatra.put(System.currentTimeMillis(), service);
		service.start();
		flash("sumatra", "the sumatra service is started");
		return ok(views.html.index.render(session()));
	}

	/**
	 * @param network
	 *            the network to generate the trend clusters
	 * @param delay
	 *            in minutes
	 * @return
	 */
	public static Result start(String network, int delay) {
		return TODO;
	}
	
	public static Result list(){
		return ok(views.html.sumatra.list.render(session()));
	}
	
	public static Result listJson(){
		
		List<SumatraDTO> list = new ArrayList<>();
		
		for (Long key : mapSumatra.keySet()) {
			list.add(new SumatraDTO(key,mapSumatra.get(key).getParams()));
		}
		return ok(Json.toJson(list));
	}
	
	public static Result stop(){
		return TODO;
	}
	
	public static Result getTrendClustersDate(Long date){
		List<TrendClusterDTO> results = TrendCluster.find(new Date(date));
		Map<Long, List<ChartSeries>> map = new HashMap<>();
		Map<Long, List<DeliveryPointDTO>> map2 = new HashMap<>();
		for (TrendClusterDTO tc : results) {
			
			if (!map.containsKey(tc.windowId)){
				map.put(tc.windowId, new ArrayList<ChartSeries>());
				map2.put(tc.windowId, new ArrayList<DeliveryPointDTO>());
			}
			
			map.get(tc.windowId).add(tc.chartSeries);
			map2.get(tc.windowId).addAll(tc.pvplantIds);
		}
		
		List<GroupClusterDTO> clusterDTO = new ArrayList<>();
		for (Long key : map.keySet()) {
			clusterDTO.add(new GroupClusterDTO(key,map.get(key),map2.get(key)));
		}
		
		return ok(Json.toJson(clusterDTO));
	}
	
	public static Result getTrendClusters(){
		List<TrendClusterDTO> results = TrendCluster.find(new Date());
		Map<Long, List<ChartSeries>> map = new HashMap<>();
		Map<Long, List<DeliveryPointDTO>> map2 = new HashMap<>();
		for (TrendClusterDTO tc : results) {
			
			if (!map.containsKey(tc.windowId)){
				map.put(tc.windowId, new ArrayList<ChartSeries>());
				map2.put(tc.windowId, new ArrayList<DeliveryPointDTO>());
			}
			
			map.get(tc.windowId).add(tc.chartSeries);
			map2.get(tc.windowId).addAll(tc.pvplantIds);
		}
		
		List<GroupClusterDTO> clusterDTO = new ArrayList<>();
		for (Long key : map.keySet()) {
			clusterDTO.add(new GroupClusterDTO(key,map.get(key),map2.get(key)));
		}
		
		return ok(Json.toJson(results));
	}
	
	public static Result results(){
		List<DateDTO> dates = TrendCluster.distinctDates();
		return ok(views.html.sumatra.results.render(session(),dates));
	}

}
