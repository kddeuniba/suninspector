package controllers;

import models.Administrator;
import models.Owner;
import controllers.forms.LoginUser;

import play.*;
import play.data.Form;
import play.mvc.*;
import views.html.defaultpages.error;

public class Login extends Controller {
	
	private static final Form<LoginUser> userform =  new Form<LoginUser>(LoginUser.class);
	
	public static Result login(){
		return ok(views.html.login.index.render(userform, session()));
	}
			
	public static Result authenticate(){
		
		Form<LoginUser> filledForm = userform.bindFromRequest();
		if (filledForm.hasErrors()){
			return badRequest(views.html.login.index.render(filledForm,session()));
		}else{
			LoginUser loginUser = filledForm.get();
			
			Administrator admin = Administrator.findByUserAndPass(loginUser.username, loginUser.password);
			
			if (admin != null){
				session().put("connected", "administrator");
				session().put("id", admin.id.toString());
				session().put("username", admin.username);
				return ok(views.html.index.render(session()));
			} 
			
			Owner owner = Owner.findByUserAndPass(loginUser.username,loginUser.password);
			if (owner != null){
				session().put("connected", "owner");
				session().put("id", owner.id.toString());
				session().put("username", owner.username);
				return ok(views.html.index.render(session()));
			} 
			
		}
		filledForm.error("wrong username or password");
		return badRequest(views.html.login.index.render(filledForm,session()));
	}
		
	public static Result logout(){
		session().clear();
		return ok(views.html.index.render(session()));
	}
}
