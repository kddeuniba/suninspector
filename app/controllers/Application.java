package controllers;

import play.*;
import play.mvc.*;
import views.html.*;

public class Application extends Controller {

	public static Result javascriptRoutes(){
		response().setContentType("text/javascript");
		return ok(
				Routes.javascriptRouter("jsRoutes", 
					controllers.routes.javascript.DeliveryPoint.list(),
					controllers.routes.javascript.Production.list(),
					controllers.routes.javascript.Sumatra.listJson(),
					controllers.routes.javascript.Production.listCSV(),
					controllers.routes.javascript.Production.listDay(),
					controllers.routes.javascript.Production.distinctDates(),
					controllers.routes.javascript.Sumatra.getTrendClusters(),
					controllers.routes.javascript.Sumatra.getTrendClustersDate()
					)
				);
	}

	public static Result index() {
		return ok(views.html.index.render(session()));
	}

}
