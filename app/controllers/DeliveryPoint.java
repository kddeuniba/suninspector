package controllers;

import java.util.List;
import models.Owner;
import clientModels.DeliveryPointDTO;
import clientModels.OwnerDTO;

import play.*;
import play.data.Form;
import play.libs.Json;
import play.mvc.*;
import views.html.*;

public class DeliveryPoint extends Controller {

	private static final Form<DeliveryPointDTO> dvForm = new Form<DeliveryPointDTO>(
			DeliveryPointDTO.class);

	@Security.Authenticated(Secured.class)
	public static Result index() {
		return ok(views.html.deliveryPoint.index.render(session()));
	}

	public static Result list() {
		List<models.DeliveryPoint> deliveryPoints = models.DeliveryPoint.all();
		return ok(Json.toJson(DeliveryPointDTO.from(deliveryPoints)));
	}

	@Security.Authenticated(Secured.class)
	public static Result create() {

		List<OwnerDTO> owners = OwnerDTO.from(Owner.all());

		return ok(views.html.deliveryPoint.create.render(dvForm, owners,
				session()));
	}

	@Security.Authenticated(Secured.class)
	public static Result save() {

		Form<DeliveryPointDTO> filledForm = dvForm.bindFromRequest();
		if (filledForm.hasErrors()) {
			List<OwnerDTO> owners = OwnerDTO.from(Owner.all());
			return badRequest(views.html.deliveryPoint.create.render(
					filledForm, owners, session()));
		}

		models.DeliveryPoint dv = DeliveryPointDTO.to(filledForm.get());
		dv.save();

		return redirect(controllers.routes.DeliveryPoint.index());
	}

}
