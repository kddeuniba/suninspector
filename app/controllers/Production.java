package controllers;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import clientModels.ProductionDTO;

import play.*;
import play.libs.Json;
import play.mvc.*;
import views.html.*;

public class Production extends Controller{

	public static Result index(){
		return ok(views.html.production.index.render(session()));
	}
	
	public static Result list(Long id){
		int size = 30;
		
		List<ProductionDTO> list = ProductionDTO.from(models.Production.listLast(id, size));
		return ok(Json.toJson(list));
	}
	
	public static Result listDay(Long id, Long day){
		List<ProductionDTO> list = ProductionDTO.from(models.Production.listDay(id, day));
		return ok(Json.toJson(list));
	}
	
	public static Result report(){
		return ok(views.html.production.report.render(session()));
	}
	
	public static Result distinctDates(Long id){
		return ok(Json.toJson(models.Production.distinctDates(id)));
	}
	
	public static Result listCSV(long id){
		
		int size = 30;
		
		List<ProductionDTO> list = ProductionDTO.from(models.Production.listLast(id, size));
		
		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(ProductionDTO.class);
		String value = "id,timestamp,production,temperature\n";
		try {
			value += mapper.writer(schema).writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ok(value);
	}
}
