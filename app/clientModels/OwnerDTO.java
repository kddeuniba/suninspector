package clientModels;

import java.util.ArrayList;
import java.util.List;

import models.Owner;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;

public class OwnerDTO {
	public String firstname;

	public String lastname;

	@MinLength(4)
	public String username;

	@Required
	@MinLength(4)
	public String password;

	public String email;

	public String company;

	public OwnerDTO() {
	}
	
	public static OwnerDTO from(Owner o){
		OwnerDTO dto = new OwnerDTO();
		dto.username = o.username;
		dto.password = o.password;
		dto.firstname = o.firstname;
		dto.lastname = o.lastname;
		dto.company = o.company;
		dto.email = o.email;
		
		return dto;
	}
	
	public static List<OwnerDTO> from(List<Owner> owners){
		List<OwnerDTO> dtos = new ArrayList<>();
		for (Owner o : owners) {
			dtos.add(from(o));
		}
		return dtos;
	}
}
