package clientModels;

import models.PVPlant;
import models.enums.Place;
import models.enums.PlantType;
import models.enums.Technology;

public class PVPlantDTO {

	public Long id;
	
	public float peakPower;
	
	public float area;
	
	public PlantType type;
	
	public Place place;
	
	public Technology technology;
	
	public float angle;
	
	public float aspectAngle;
	
	public PVPlantDTO() {
	}

	public static PVPlantDTO from(PVPlant pv) {
		
		PVPlantDTO dto = new PVPlantDTO();
		dto.id = pv.id;
		dto.peakPower = pv.peakPower;
		dto.area = pv.area;
		dto.type = pv.type;
		dto.place = pv.place;
		dto.technology = pv.technology;
		dto.angle = pv.angle;
		dto.aspectAngle = pv.aspectAngle;
		return dto;
	}
	
	
}
