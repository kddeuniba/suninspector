package clientModels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import play.data.validation.Constraints.Required;

import models.DeliveryPoint;
import models.Owner;
import models.PVPlant;
import models.enums.Status;

/**
 * @author fabiofumarola
 * 
 */
public class DeliveryPointDTO implements Serializable{

	public Long id;
	
	public String idString;

	public String street;

	public String status;

	public String owner;
	
	@Required
	public String position;
	
	public PVPlantDTO pvPlant;
	
	public List<PVPlantDTO> pvPlants = new ArrayList<PVPlantDTO>();
	
	public Long number;
	
	public DeliveryPointDTO() {
		pvPlant = new PVPlantDTO();
		pvPlants = new ArrayList<>();
	}

	public static DeliveryPointDTO from(DeliveryPoint dp) {
		DeliveryPointDTO dto = new DeliveryPointDTO();
		dto.id = dp.id;
		dto.idString = "<a href=\"\">" + dto.id +  "</a>"; 
		dto.street = dp.street;
		dto.owner = dp.owner.username;
		dto.status = dp.status.toString();
		dto.position = dp.position;
		dto.status = dp.status.toString();
		for (PVPlant pv : dp.plants) {
			dto.pvPlants.add(PVPlantDTO.from(pv));
		}
		if (dto.pvPlants.size() > 0)
			dto.pvPlant = dto.pvPlants.get(0); 
		
		return dto;
	}
	
	private static DeliveryPointDTO from(DeliveryPoint dp, PVPlant pv) {
		DeliveryPointDTO dto = from(dp);
		dto.pvPlant = PVPlantDTO.from(pv);
		return dto;
	}
	
	public static List<DeliveryPointDTO> from(List<DeliveryPoint> dps){
		List<DeliveryPointDTO> list = new ArrayList<>();
		
		for (DeliveryPoint dp : dps) {
			for (PVPlant pv : dp.plants) {
				list.add(DeliveryPointDTO.from(dp,pv));
			}
		}
		
		return list;
	}

	public static DeliveryPoint to(DeliveryPointDTO dvDTO) {
		
		DeliveryPoint dv = new DeliveryPoint();
		dv.owner = Owner.findByUsername(dvDTO.owner);
		dv.position = dvDTO.position;
		dv.street = dvDTO.street;
		dv.status = Status.stopped;
		
		PVPlant pv = new PVPlant();
		pv.peakPower = dvDTO.pvPlant.peakPower;
		pv.area = dvDTO.pvPlant.area;
		pv.place = dvDTO.pvPlant.place;
		pv.technology = dvDTO.pvPlant.technology;
		pv.type = dvDTO.pvPlant.type;
		pv.angle = dvDTO.pvPlant.angle;
		pv.aspectAngle = dvDTO.pvPlant.aspectAngle;
	
		dv.plants.add(pv);
		
		return dv;
	}
}
