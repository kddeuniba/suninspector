package clientModels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import sumatra.model.PolyLine;

public class TrendClusterDTO implements Serializable{
	
	public Long id;
	
	public int number;

	public Date startTime;

	public Date endTime;

	public ChartSeries chartSeries = new ChartSeries();

	public List<DeliveryPointDTO> pvplantIds = new ArrayList<>();

	public PolyLine polyline = new PolyLine();

	public Long windowId;
	
}
