package clientModels;

import java.util.ArrayList;
import java.util.List;

public class ChartSeries {

	public String key;
	
	public List<Object[]> values = new ArrayList<>();
	
	public ChartSeries() {
	}

	public ChartSeries(String key) {
		this.key = key;
	}
	
	public void add(Long x, Double y){
		this.values.add(new Object[]{x,y});
	}
}
