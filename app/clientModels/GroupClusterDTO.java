package clientModels;

import java.util.List;

public class GroupClusterDTO {

	public Long id;
	
	public List<DeliveryPointDTO> clusters;
	
	public List<ChartSeries> charts;
	
	public GroupClusterDTO() {
	}

	public GroupClusterDTO(Long key, List<ChartSeries> charts, List<DeliveryPointDTO> clusters ) {
		id = key;
		this.charts = charts;
		this.clusters = clusters;
	}
}
