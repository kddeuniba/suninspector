package clientModels;

import java.util.Date;

import controllers.forms.TrendForm;

public class SumatraDTO {

	public long id;
	
	public String network;
	
	public Date startingTime;

	public int intervalMinute;

	public int windowSize;

	public float threshold;
	
	public double maxDistanceEdge; 
	
	public SumatraDTO() {
	}

	public SumatraDTO(Long key, TrendForm trendForm) {
		id = key;
		network = trendForm.network;
		startingTime = trendForm.startingTime;
		intervalMinute = trendForm.intervalMinute;
		windowSize = trendForm.windowSize;
		threshold = trendForm.threshold;
		maxDistanceEdge = trendForm.maxDistanceEdge;
	}
}
