package clientModels;

import it.kdde.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import models.Production;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.common.collect.Collections2;

public class ProductionDTO implements Serializable{
	
	public long pvPlantId;
	
	public String timestamp;
	
	public double energy;
	
	public float temperature;
	
	public ProductionDTO() {
	}
	
	public ProductionDTO(Production prod){
		this.pvPlantId = prod.pvPlantId;
		this.timestamp = DateTimeFormat.forPattern("dd-MM-YYYY HH:mm").print(prod.timestamp.getTime());
		//this.timestamp = new DateTime(prod.timestamp.getTime()).toDateTimeISO().toString();
		this.energy = prod.energy;
		this.temperature = prod.temperature;
	}

	public static List<ProductionDTO> from(List<models.Production> productions) {
		
		List<ProductionDTO> list = new ArrayList<>();
		for (Production prod : productions) {
			list.add(new ProductionDTO(prod));
		}
		
		Collections.sort(list, new Comparator<ProductionDTO>(){

			@Override
			public int compare(ProductionDTO o1, ProductionDTO o2) {
				return o1.timestamp.compareTo(o2.timestamp);
			}
			
		});
				
		return list;
	}
}
