package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import models.enums.Status;

import play.db.ebean.Model;

@Entity
public class DeliveryPoint extends Model{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	@ManyToOne
	@JoinColumn(name = "owner")
	public Owner owner;
	
	public String street;
	
	public String position;
	
	public boolean active;
	
	public Status status = Status.stopped;
	
	@OneToMany(mappedBy = "deliveryPoint", cascade = CascadeType.ALL)
	public List<PVPlant> plants;
	
	public DeliveryPoint() {
	}
	
	private static Finder<Long,DeliveryPoint> finder = new Finder<>(Long.class, DeliveryPoint.class);
	
	
	public static List<DeliveryPoint> all(){
		return finder.fetch("owner","username").findList();
	}


	public static DeliveryPoint findById(Long id) {
		return finder.byId(id);
	}

	@Override
	public String toString() {
		return "DeliveryPoint [id=" + id + ", owner=" + owner + ", street="
				+ street + ", position=" + position + ", active=" + active
				+ ", status=" + status + ", plants=" + plants + "]";
	}
	
	
}
