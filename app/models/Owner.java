package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
public class Owner extends Model{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String firstname;
	
	public String lastname;
	
	@MinLength(4)
	public String username;
	
	@Required
	@MinLength(4)
	public String password;
	
	public String email;
	
	public String company;
	
	@OneToMany(mappedBy = "owner")
	public List<DeliveryPoint> deliveryPoints;
	
	public Owner() {
	}
	
	private static Finder<Long, Owner> finder = new Finder<>(Long.class, Owner.class);

	public static Owner findByUserAndPass(String user, String pass) {
		String query = "find Owner where username = :username and password = :password limit 1";
		return finder.setQuery(query).setParameter("username", user).setParameter("password", pass).findUnique();
	}

	public static List<Owner> all() {
		return finder.findList();
	}

	public static Owner findByUsername(String username) {
		return finder.where().eq("username", username).findList().get(0);
	}
	
	@Override
	public String toString() {
		return "Owner [id=" + id + ", firstname=" + firstname + ", lastname="
				+ lastname + ", username=" + username + ", password="
				+ password + ", email=" + email + ", company=" + company
				+ ", deliveryPoints=" + deliveryPoints + "]";
	}	
}
