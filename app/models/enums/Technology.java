package models.enums;


import java.util.HashMap;
import java.util.Map;

import com.avaje.ebean.annotation.EnumValue;

public enum Technology {
	
//	m_Si("monocristalline"), p_Si("polycrystalline"), a_Si("amorphous"), CIS("CIS"), CIGS("CIGS"),
//	CdTe("CdTe"), HIT("Ibride"), DSC("nanocristalline");

	@EnumValue("monocristalline")
	m_Si,
	
	@EnumValue("polycrystalline")
	p_Si,
	
	@EnumValue("amorphous")
	a_Si,
	
	@EnumValue("CIS")
	CIS,
	
	@EnumValue("CIGS")
	CIGS,
	
	@EnumValue("CdTe")
	CdTe,
	
	@EnumValue("Ibride")
	HIT,
	
	@EnumValue("nanocristalline")
	DSC;
	
	public static Map<String, String> mapValues(){
		Technology[] technologies = Technology.values();
		
		Map<String, String> options = new HashMap<>();
		
		for (Technology t : technologies) {
			options.put(t.name(), t.name());
		}	
		return options;
		
	}
	
}
