package models.enums;

import java.util.HashMap;
import java.util.Map;

import com.avaje.ebean.annotation.EnumValue;

public enum Status {

	@EnumValue("simulated")
	simulated,
    
	@EnumValue("stopped")
	stopped,
	
	@EnumValue("running")
	running,
	
	@EnumValue("reset")
	reset;
	
	public static Map<String, String> mapValues(){
		Status[] type = Status.values();
		
		Map<String, String> options = new HashMap<String, String>();
		
		for (Status t : type) {
			options.put(t.name(), t.name());
		}
		
		return options;
		
	}
}
