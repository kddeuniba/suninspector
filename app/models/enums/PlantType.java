package models.enums;

import java.util.HashMap;
import java.util.Map;

import com.avaje.ebean.annotation.EnumValue;

public enum PlantType {

	@EnumValue("grid-connected")
	grid_connected,
    
	@EnumValue("stand-alone")
	stand_alone;
	
	public static Map<String, String> mapValues(){
		PlantType[] type = PlantType.values();
		
		Map<String, String> options = new HashMap<>();
		
		for (PlantType t : type) {
			options.put(t.name(), t.name());
		}
		
		return options;
		
	}
}
