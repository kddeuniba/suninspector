package models.enums;

import java.util.HashMap;
import java.util.Map;

import com.avaje.ebean.annotation.EnumValue;

public enum Place {
	
	@EnumValue("free")
	free, 
	
	@EnumValue("building")
	building;
	
	public static Map<String, String> mapValues(){
		Place[] places = Place.values();
		
		Map<String, String> options = new HashMap<>();
		
		for (Place p : places) {
			options.put(p.name(), p.name());
		}
		
		return options;
		
	}
}
