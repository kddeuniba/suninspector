package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.JsonNode;

import clientModels.ChartSeries;
import clientModels.DateDTO;
import clientModels.DeliveryPointDTO;
import clientModels.TrendClusterDTO;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import play.db.ebean.Model;
import play.libs.Json;
import sumatra.model.PolyLine;
import sumatra.model.PolyPoint;

@Entity
public class TrendCluster extends Model{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public Long windowId;
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date startTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date endTime;
	
	//to be serialized as json
	public String polyline;
	
	//to be serialized as json
	public String pvplantIds;
	
	public int clusterNumber;
	
	public TrendCluster() {
	}
	
	public static Finder<Long, TrendCluster> finder = new Finder<>(Long.class, TrendCluster.class);

	public static void save(List<sumatra.model.TrendCluster> clusters) {		
		ObjectMapper m = new ObjectMapper();
		
		for (sumatra.model.TrendCluster tc : clusters) {
			TrendCluster cluster = new TrendCluster();
			cluster.clusterNumber = tc.getClusterId();
			cluster.windowId = tc.getWindowId();
			cluster.startTime = new Date(tc.getWindow().getStartingTime());
			cluster.endTime = new Date(tc.getWindow().getEndingTime());
			try {
				cluster.polyline = m.writeValueAsString(tc.getPolyLine());
				cluster.pvplantIds = m.writeValueAsString(tc.getPlantsIds());
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Ebean.save(cluster);
		}
	}
	
	/**
	 * @param date
	 * @return all the trend cluster for a date
	 */
	public static List<TrendClusterDTO> find(Date date){
		
		List<TrendCluster> clusters = finder.where().eq("\"start_time\"::date", date).order().asc("start_time").findList();
		
		List<TrendClusterDTO> result = new ArrayList<>();
		
		for (TrendCluster tc : clusters) {
			TrendClusterDTO dto = new TrendClusterDTO();
			dto.id = tc.id;
			dto.number = tc.clusterNumber;
			dto.startTime = tc.startTime;
			dto.endTime = tc.endTime;
			dto.windowId = tc.windowId;
			JsonNode pLine = Json.parse(tc.polyline);
	
			Map<Long, List<Double>> values = new TreeMap<>();
			PolyLine poly = Json.fromJson(pLine, PolyLine.class);
			dto.chartSeries.key = Integer.toString(dto.number);
			for (PolyPoint pl : poly.getPoints()) {
				dto.chartSeries.add(pl.getTime(),pl.getValue());
			}
			dto.polyline = poly;
			JsonNode pvIds = Json.parse(tc.pvplantIds);
			Set<Integer> setIds = Json.fromJson(pvIds, Set.class);
			for (Integer i : setIds) {
				DeliveryPointDTO deliveryPointDTO = DeliveryPointDTO.from(DeliveryPoint.findById(i.longValue()));
				deliveryPointDTO.number = (long) dto.number;
				dto.pvplantIds.add(deliveryPointDTO);
			}
			result.add(dto);
		}
		return result;
	}
	
	public static List<DateDTO> distinctDates(){
		
		String sSql = "select distinct \"start_time\"::date from trend_cluster order by \"start_time\"::date asc";
		RawSql rawSql = RawSqlBuilder.parse(sSql).columnMapping("\"start_time\"::date", "date").create();
		Query<DateDTO> query = Ebean.find(DateDTO.class).setRawSql(rawSql);
		List<DateDTO> result = query.findList();
		for (DateDTO dateDTO : result) {
			dateDTO.dateString = dateDTO.date.toString();
		}
		return result;
	}
}
