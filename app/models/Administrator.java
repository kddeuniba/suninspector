package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import play.data.validation.Constraints.Max;
import play.data.validation.Constraints.Min;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

@Entity
public class Administrator extends Model {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;

	@Required
	public String firstname;

	@Required
	public String lastname;

	@Required
	@Min(5)
	@Max(15)
	public String username;

	public String password;

	public String email;

	public Administrator() {
	}

	private static Finder<Long, Administrator> finder = new Finder<Long, Administrator>(
			Long.class, Administrator.class);

	public static Administrator findByUserAndPass(String username,
			String password) {
		String query = "find Administrator where username = :username and password = :password limit 1";
		return finder.setQuery(query).setParameter("username", username).setParameter("password", password).findUnique();

	}

}
