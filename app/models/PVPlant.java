package models;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import models.enums.DbType;
import models.enums.Place;
import models.enums.PlantType;
import models.enums.Technology;
import play.db.ebean.Model;
import sumatra.Utils;
import sumatra.model.GeoPoint;

@Entity
public class PVPlant extends Model{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public float peakPower;
	
	public float area;
	
	public PlantType type;
	
	public Place place;
	
	public Technology technology;
	
	public float angle;
	
	public float aspectAngle;
	
	private transient GeoPoint point;
	
	@ManyToOne
	@JoinColumn(name="deliveryPoint")
	public DeliveryPoint deliveryPoint;
	
	public PVPlant() {
	}
	
	private static Finder<Long, PVPlant> finder = new Finder<>(Long.class, PVPlant.class);

	public static PVPlant findById(Long id) {
		return finder.byId(id);
	}

	public String asGetUrl() {
		StringBuffer buf = new StringBuffer();
		buf.append(id);
		buf.append("/");
		String pos = "(40.7059773,%2017.66250009999999)";
		try {
			if (deliveryPoint.position.length() > 0)
				pos = URLEncoder.encode(deliveryPoint.position, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buf.append(pos);
		buf.append("/");
		buf.append(peakPower);
		buf.append("/");
		buf.append(area);
		buf.append("/");
		buf.append(type.toString());
		buf.append("/");
		buf.append(place);
		buf.append("/");
		buf.append(technology);
		buf.append("/");
		buf.append(DbType.pvgis_classic);
		buf.append("/");
		buf.append(angle);
		buf.append("/");
		buf.append(aspectAngle);
		buf.append("/");
		return buf.toString();
	}

	public static List<PVPlant> findbByIds(Set<Long> pvPlantIds) {
		
		List<PVPlant> list = finder.where().in("id", pvPlantIds).findList();
		for (PVPlant pvPlant : list) {
			pvPlant.point = Utils.getPoint(pvPlant.deliveryPoint.position);
		}
		
		return list;
	}
	
	public GeoPoint getPoint() {
		return point;
	}

	@Override
	public String toString() {
		return "PVPlant [id=" + id + ", peakPower=" + peakPower + ", area="
				+ area + ", type=" + type + ", place=" + place
				+ ", technology=" + technology + ", angle=" + angle
				+ ", aspectAngle=" + aspectAngle + ", deliveryPoint="
				+ deliveryPoint + "]";
	}
}
