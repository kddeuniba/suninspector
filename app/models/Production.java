package models;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import clientModels.DateDTO;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;

import play.db.ebean.Model;

@Entity
@IdClass(ProductionPK.class)
public class Production extends Model{

	@Id
	public Long pvPlantId;
	
	@Id
	@Temporal(TemporalType.TIMESTAMP)
	public Date timestamp;
	
	public double energy;
	
	public float temperature;
	
	public Production() {
	}
	
	public Production(Long id, Long timestamp, Double e, Float temp) {
		this.pvPlantId = id;
		this.timestamp = new Date(timestamp);
		this.energy = e;
		this.temperature = temp;
	}

	private static Finder<Long, Production> finder = new Finder<>(Long.class, Production.class);

	public static List<Production> findByPvIdAndTimestamp(Long id, Date time) {
		return finder.where().eq("pvPlantId",id).eq("timestamp", time).findList();
	}
	
	public static List<Production> listLast(Long id, int size){
		
		List<Production> productions = finder.where().eq("pvPlantId", id).orderBy().
				desc("timestamp").findPagingList(size).getAsList();
		return productions;
	}

	public static List<Production> findInPeriod(Long startingTime,
			Long endTime, String network, boolean sumatra) {	
		List<Production> list = finder.where().ge("timestamp", new Date(startingTime)).
		le("timestamp", new Date(endTime)).findList();
		
		if (sumatra){
			for (Production production : list) {
				PVPlant plant =PVPlant.findById(production.pvPlantId);
				production.energy = production.energy / plant.peakPower;
			}
		}
		
		return finder.where().ge("timestamp", new Date(startingTime)).
				le("timestamp", new Date(endTime)).findList();
	}

	public static int findCountGreaterThan(Long end) {
		return finder.where().ge("timestamp", new Date(end)).findRowCount();
	}
	
	public static List<DateDTO> distinctDates(Long id){
		
		String sSql = "select distinct \"timestamp\"::date from production where pv_plant_id=" + id + " order by \"timestamp\"::date asc";
		RawSql rawSql = RawSqlBuilder.parse(sSql).columnMapping("\"timestamp\"::date", "date").create();
		Query<DateDTO> query = Ebean.find(DateDTO.class).setRawSql(rawSql);
		List<DateDTO> result = query.findList();
	
		return result;
	}
	
	public static List<Production> listDay(Long id, long date){		
		
		//end of the day
		long endDate = date + (24 * 60 * 60 * 1000) -1;
		
		List<Production> productions = finder.where().eq("pvPlantId", id).gt("timestamp", new Date(date)).
				lt("timestamp", new Date(endDate)).findList();
				
		return productions;
	}

	public String toString() {
		return "Production [pvPlantId=" + pvPlantId + ", timestamp="
				+ timestamp + ", energy=" + energy + ", temperature="
				+ temperature + "]";
	}
}
