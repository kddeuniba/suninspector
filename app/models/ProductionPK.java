package models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class ProductionPK implements Serializable {

	public Long pvPlantId;
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date timestamp;
	
	public ProductionPK() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((pvPlantId == null) ? 0 : pvPlantId.hashCode());
		result = prime * result
				+ ((timestamp == null) ? 0 : timestamp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductionPK other = (ProductionPK) obj;
		if (pvPlantId == null) {
			if (other.pvPlantId != null)
				return false;
		} else if (!pvPlantId.equals(other.pvPlantId))
			return false;
		if (timestamp == null) {
			if (other.timestamp != null)
				return false;
		} else if (!timestamp.equals(other.timestamp))
			return false;
		return true;
	}
	
	
}
