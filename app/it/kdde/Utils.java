package it.kdde;

import java.util.Date;

import org.joda.time.DateTime;

public class Utils {


	public static String getDate(Date d) {
		DateTime time = new DateTime(d.getTime());
		String value = time.getDayOfMonth() + "-" + time.getMonthOfYear() + "-"
				+ time.getYear() + " " + time.getHourOfDay() + ":"
				+ time.getMinuteOfHour();
		return value;
	}
	
}
