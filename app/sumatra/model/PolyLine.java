package sumatra.model;

import java.util.ArrayList;
import java.util.List;

public class PolyLine {

	private List<PolyPoint> points;
	
	public PolyLine() {
		points = new ArrayList<>();
	}
	
	public List<PolyPoint> getPoints() {
		return points;
	}
	
	public void setPoints(List<PolyPoint> points) {
		this.points = points;
	}
	
	public void add(PolyPoint p){
		points.add(p);
	}

	@Override
	public String toString() {
		return "PolyLine [points=" + points + "]";
	}
	
}
