package sumatra.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import sumatra.Utils;

/**
 * @author fabiofumarola
 *
 */
public class Window implements Serializable, Iterable<Snapshot> {

	private static int count = 0;
	
	private long startingTime;
	
	private long endingTime;

	private int id;
	private Snapshot[] elements;
	private Set<Long> pvPlantIds;
	private int n;
	private int productionCount = 0;

	public Window(long startingTime, long snapshotTimeRange, int windowsSize,
			String network) {
		this.startingTime = startingTime;
		id = count++;
		elements = new Snapshot[windowsSize];
		long startTime = startingTime;
		long endTime = startTime + snapshotTimeRange;
		pvPlantIds = new HashSet<>();
		
		for (int i = 0; i < windowsSize; i++) {
			Snapshot snapshot = new Snapshot(startTime, endTime, network);
			productionCount += snapshot.size();
			add(snapshot);
			pvPlantIds.addAll(snapshot.keySet());
			//increase time
			startTime = endTime;
			endTime = endTime + snapshotTimeRange;
		}
		this.endingTime = endTime;
	}

	public void add(Snapshot s) {
		if (n > elements.length)
			throw new RuntimeException("n is greater than elements.size");

		elements[n++] = s;
	}

	public Snapshot getSnapshot(int i) {
		if (i > elements.length)
			throw new RuntimeException();
		
		return elements[i];
	}
	
	public Set<Long> getPvPlantIds() {
		return pvPlantIds;
	}
	
	public int getId() {
		return id;
	}

	public int length() {
		return n;
	}
	
	/**
	 * @param nodeA
	 * @param nodeB
	 * @param delta
	 * @return two nodes are neighbors if all the measures of these nodes have
	 *         distance less than delta
	 */
	public boolean areNeighbors(long nodeA, long nodeB, double delta) {
		Double valueA;
		Double valueB;

		for (Snapshot s : elements) {
				valueA = s.getValue(nodeA);
				valueB = s.getValue(nodeB);
				
				if (valueA == null || valueB == null)
					return false;
				
				if (Utils.euclideanDistance(valueA, valueB) > delta)
					return false;
		}
		return true;
	}

	@Override
	public Iterator<Snapshot> iterator() {
		return new WindowIterator(this);
	}

	private class WindowIterator implements Iterator<Snapshot> {

		private Window window;

		private int next;

		public WindowIterator(Window window) {
			this.window = window;
		}

		@Override
		public boolean hasNext() {
			return next < window.n;
		}

		@Override
		public Snapshot next() {
			return window.elements[next++];
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
		}
	}

	public Snapshot[] getElements() {
		return elements;
	}
	
	public long getEndingTime() {
		return endingTime;
	}
	
	public long getStartingTime() {
		return startingTime;
	}
	
	public int getNumObservations(){
		return productionCount;
	}

	@Override
	public String toString() {
		return "Window [startingTime=" + startingTime + ", endingTime="
				+ endingTime + ", id=" + id + ", elements="
				+ Arrays.toString(elements) + ", pvPlantIds=" + pvPlantIds
				+ ", productionCount=" + productionCount + "]";
	}

	public String timeBound() {
		return new Date(startingTime) + " " + new Date(endingTime);
	}

}
