package sumatra.model;

public class GeoPoint {

	private double x;
	
	private double y;
	
	public GeoPoint(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
    public  double getDistance(GeoPoint p2)
    {
        double dX = this.x - p2.x;
        double dY = this.y - p2.y;
       
        return Math.sqrt(dX * dX + dY * dY);
    }

	@Override
	public String toString() {
		return "GeoPoint [x=" + x + ", y=" + y + "]";
	}
	
}
