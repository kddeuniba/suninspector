package sumatra.model;

public class PolyPoint {
	
	private long time;
	private double value;
	
	public PolyPoint() {
	}
	
	public PolyPoint(long startTime, double value) {
		this.time = startTime;
		this.value = value;
	}

	public long getTime() {
		return time;
	}
	
	public double getValue() {
		return value;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
	
	public void setTime(long time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "PolyPoint [time=" + time + ", value=" + value + "]";
	}
	
}
