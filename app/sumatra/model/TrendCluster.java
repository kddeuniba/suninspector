package sumatra.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TrendCluster {
	
	private long windowId;
		
	private int clusterId;
	
	private Window window;

	private PolyLine polyLine;
	
	private Set<Long> plantsIds =new HashSet<Long>();
	
	public TrendCluster(long windowId, int clusterId, Window window) {
		this.clusterId = clusterId;
		this.window = window;
		this.windowId = windowId;
	}

	public Set<Long> getPlantsIds() {
		return plantsIds;
	}
	
	public PolyLine getPolyLine() {
		return polyLine;
	}
	
	public Window getWindow() {
		return window;
	}
	
	public void setWindow(Window window) {
		this.window = window;
	}
	
	public void setPlants(List<Long> plantsIds) {
		this.plantsIds.addAll(plantsIds);
	}
	
	public void setPolyLine(PolyLine polyLine) {
		this.polyLine = polyLine;
	}
	
	public int getClusterId() {
		return clusterId;
	}
	
	public long getWindowId() {
		return windowId;
	}
	
	public void setWindowId(long windowId) {
		this.windowId = windowId;
	}

	@Override
	public String toString() {
		return "TrendCluster [windowId=" + windowId + ", clusterId="
				+ clusterId + ", window=" + window + ", polyLine=" + polyLine
				+ ", plantsIds=" + plantsIds + "]";
	}	
}
