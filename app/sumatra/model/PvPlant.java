package sumatra.model;

import java.util.ArrayList;
import java.util.List;

import models.Production;

public class PvPlant {

	private long id;
	
	private double peakPower;
	
	private List<Production> productions = new ArrayList<>();
	
	public PvPlant() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getPeakPower() {
		return peakPower;
	}

	public void setPeakPower(double peakPower) {
		this.peakPower = peakPower;
	}

	public List<Production> getProductions() {
		return productions;
	}

	public void setProductions(List<Production> productions) {
		this.productions = productions;
	}

	@Override
	public String toString() {
		return "PvPlant [id=" + id + ", peakPower=" + peakPower
				+ ", productions=" + productions + "]";
	}
	
}
