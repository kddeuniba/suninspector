package sumatra.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import models.Production;

public class Snapshot {
	
	private long startTime;
	
	private long endTime;
	
	private Map<Long, Production> elements = new HashMap<Long, Production>();
	
	public Snapshot() {
	}

	public Snapshot(long startTime, long endTime, String network) {
		this.startTime = startTime;
		this.endTime = endTime;
		List<Production> productions = Production.findInPeriod(startTime,endTime,network,true);
		
		for (Production production : productions) {
			elements.put(production.pvPlantId, production);
		}
	}
	
	public Double getValue(long key){
		Production p = elements.get(key);
		if (p != null)
			return elements.get(key).energy;
		else return 0D;
	}
	
	public int size(){
		return elements.size();
	}

	public Collection<Long> keySet() {
		return elements.keySet();
	}
	

	
	public long getStartTime() {
		return startTime;
	}
	
	public long getEndTime() {
		return endTime;
	}
	
	public List<Double> getProductions(){
		List<Double> prods = new ArrayList<>(elements.size());
		for (Production p : elements.values()) {
			prods.add(p.energy);
		}
		
		return prods;
	}

	public List<Double> getProductions(Collection<Long> groupNodes) {
		List<Double> prods = new ArrayList<>();
		for (Long l : groupNodes) {
			if (elements.containsKey(l))
				prods.add(elements.get(l).energy);
			else 
				prods.add(0D);
		}
		return prods;
	}

	@Override
	public String toString() {
		return "Snapshot [startTime=" + startTime + ", endTime=" + endTime
				+ ", elements=" + elements + "]";
	}
	
	
}
