package sumatra;


import java.util.Iterator;

import models.Production;

import sumatra.model.Window;

public class WindowsIterator implements Iterator<Window> {

	public WindowProducer windowProducer;
	
	public Window currentWindow;

	private long startingTime;

	public WindowsIterator(WindowProducer wp) {

		this.windowProducer = wp;
		startingTime = windowProducer.startingTime;
		// get the current window from the db
		currentWindow = createWindow();
	}
	
	/**
	 * check if there are rilevation with timestamp greater that the end of the current window
	 */
	public boolean checkCanBeProduced(){
		
		Long end = startingTime + windowProducer.snapshotTimeRange
				* windowProducer.windowsSize;
		return Production.findCountGreaterThan(end) > 0;
	}

	@Override
	public boolean hasNext() {
		return currentWindow.getNumObservations() > 0;
	}

	@Override
	public Window next() {
		Window temp = currentWindow;
		// get the current window from the db
		currentWindow = createWindow();
		return temp;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
	}

	private Window createWindow() {
		Window window = new Window(startingTime,windowProducer.snapshotTimeRange, windowProducer.windowsSize,
				windowProducer.network);

		startingTime = window.getEndingTime() + 1;
		return window;
	}
}

