package sumatra;

import java.util.List;
import java.util.concurrent.TimeUnit;

import models.PVPlant;

import sumatra.model.TrendCluster;
import sumatra.model.Window;

import com.google.common.util.concurrent.AbstractScheduledService;

import controllers.forms.TrendForm;

public class SumatraService extends AbstractScheduledService {

	/**
	 * the delay in minutes
	 */
	private int delay;
	
	private int fails;

	private TrendForm params;

	private WindowsIterator producer;
	
	public SumatraService(int delay, TrendForm trendForm) {
		this.delay = delay;
		this.params = trendForm;
		producer = (WindowsIterator) new WindowProducer(trendForm.startingTime.getTime(),
				trendForm.windowSize, trendForm.intervalMinute,
				trendForm.network).iterator();
	}
	
	public TrendForm getParams() {
		return params;
	}

	@Override
	protected void runOneIteration() throws Exception {
//		
		if (!producer.checkCanBeProduced())
			return; //skip the current iteration
		
//		if (!producer.hasNext()){
//			fails++;
//			if (fails > 10000000000L){
//				this.stop();
//				System.out.println("stopped");
//			}else
//			return;
//		}
		
		Window window = producer.next();
		while (window.getNumObservations() == 0){
			System.out.println("no obs window " + window.timeBound());
			window = producer.next();
		}

		try {
			System.out.println("obs window " + window.timeBound());
			List<PVPlant> plants = PVPlant.findbByIds(window.getPvPlantIds());
			Sumatra sumatra = new Sumatra(window,plants, params.maxDistanceEdge, params.threshold);
			List<TrendCluster> clusters = sumatra.cluster();
			//save the clusters to the db
			models.TrendCluster.save(clusters);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected Scheduler scheduler() {
		return Scheduler.newFixedRateSchedule(0, 5, TimeUnit.SECONDS);
	}

}
