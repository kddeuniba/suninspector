package sumatra;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;


import models.PVPlant;
import sumatra.model.*;

public class Sumatra {
	
	private int windowId;

	private Window window;

	private List<PVPlant> plants;

	private float delta;

	private Set<Long> clustered;

	private List<TrendCluster> clusters;

	private int countClusterId;

	private Map<Long, Set<Long>> adjacentList;
	
	private static int count = 1;

	public Sumatra(Window window, List<PVPlant> plants, double maxDistanceEdge,
			float delta) {
		this.windowId = count++;
		this.window = window;
		this.plants = plants;
		this.delta = delta;
		clustered = new HashSet<>();
		adjacentList = getAdjacentList(plants, maxDistanceEdge);
		clusters = new ArrayList<>();
	}

	public List<TrendCluster> cluster() {

		for (PVPlant plant : plants) {
			if (!clustered.contains(plant.id)) {
				discoverClusters(plant.id);
			}
		}

		for (TrendCluster cluster : clusters) {
			PolyLine p = Utils.computePolyline(cluster.getPlantsIds(), window);
			cluster.setPolyLine(p);
		}
		return clusters;
	}

	private void discoverClusters(Long plantId) {
		Queue<Long> queue = new ConcurrentLinkedQueue<Long>();
		// TrendCluster trendCluster = new TrendCluster();
		TrendCluster trendCluster = new TrendCluster(windowId,countClusterId++,window);
		queue.add(plantId);// mette in coda un nodo per volta
		trendCluster.getPlantsIds().add(plantId);
		clustered.add(plantId);

		while (!queue.isEmpty()) {
			Long nodeA = queue.remove();
			// get all the spatial neighbors of nId
			if (!adjacentList.containsKey(nodeA))
				continue;
			
			Set<Long> adjacent = adjacentList.get(nodeA);			
			List<Long> neighbors = new ArrayList<>();

			// for each node in the spatial neighbors check if they are measure
			// close to \delta
			for (Long nodeB : adjacent) {
				if (window.areNeighbors(nodeA, nodeB, delta))
					if (!clustered.contains(nodeB))
						neighbors.add(nodeB);
			}

			expandCluster(trendCluster, neighbors, false);

			for (Long neighbour : neighbors) {
				if (trendCluster.getPlantsIds().contains(neighbour)) {
					queue.add(neighbour);
				}
			}
		}
		clusters.add(trendCluster);
	}

	private void expandCluster(TrendCluster cluster, List<Long> neighbors, boolean stopRecursion) {
		
		// create a list with all the cluster nodes
		List<Long> groupNodes = new ArrayList<>(cluster.getPlantsIds());
		// add all the candidates to the groupNodes
		groupNodes.addAll(neighbors);

		// compute a polyline for all the groupNodes
		PolyLine prototypePolyline = Utils.computePolyline(groupNodes,
				window);

		boolean similar = false;
		for (int i = 0; i < window.length(); i++) {

			Snapshot s = window.getSnapshot(i);

			List<Double> rilevations = s.getProductions(groupNodes);

			double min = Collections.min(rilevations);
			double max = Collections.max(rilevations);

			double pSnapshotValue = prototypePolyline.getPoints().get(i).getValue();

			if ((Math.abs(max - pSnapshotValue) <= delta) && (Math.abs(min - pSnapshotValue) <= delta)) {
				similar = true;
			} else {
				similar = false;
				break; // break if there is any value that is not in the
						// interval abs(max - value) abs(min -value)
			}
		}

		if (similar) {
			for (Long n : neighbors) {
				clustered.add(n);
				cluster.getPlantsIds().add(n);
			}

		} else if (!stopRecursion) {
			for (Long n : neighbors) {
				List<Long> list = new ArrayList<>();
				list.add(n);
				expandCluster(cluster, list, true);
			}
		}
	}

	private static Map<Long, Set<Long>> getAdjacentList(List<PVPlant> plants,
			double maxDistanceEdge) {

		Map<Long, Set<Long>> adjacentList = new HashMap<>();

		for (int i = 0; i < plants.size(); i++) {
			Long start = plants.get(i).id;
			GeoPoint src = plants.get(i).getPoint();

			Set<Long> list = new HashSet<>();
						
			for (int j = i + 1; j < plants.size() -1; j++) {
				Long end = plants.get(j).id;
				GeoPoint dst = plants.get(j).getPoint();
				if (src.getDistance(dst) <= maxDistanceEdge)
					list.add(end);
			}
			adjacentList.put(start, list);
		}

		return adjacentList;
	}
}
