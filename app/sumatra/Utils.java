package sumatra;


import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sumatra.model.GeoPoint;
import sumatra.model.PolyLine;
import sumatra.model.PolyPoint;
import sumatra.model.Snapshot;
import sumatra.model.Window;

public class Utils {

	public static double euclideanDistance(Double valueA, Double valueB) {
		return Math.sqrt(Math.pow(valueA - valueB, 2.0));
	}

	public static GeoPoint getPoint(String position) {
		
		Double[] dd = getLatAndLong(position);
		return new GeoPoint(dd[0], dd[1]);
	}
	
	/**
	 * @param coordinates
	 * @return and array with latitude and longitude
	 */
	public static Double[] getLatAndLong(String coordinates) {
		
		String regex = "(\\d+\\.?\\d+)";

		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(coordinates);

		Double[] coord = new Double[2];
		int i = 0;
		while (m.find()) {
			coord[i] = Double.parseDouble(m.group());
			i++;
		}

		return coord;
	}

	public static PolyLine computePolyline(Collection<Long> groupNodes, Window window) {
		PolyLine p = new PolyLine();
		for (Snapshot s : window) {
			p.add(new PolyPoint(s.getStartTime(), median(s.getProductions(groupNodes))));
		}
		return p;
	}
	
	private static double median(List<Double> rilevations) {
		return getQuartil(rilevations, 2);
	}
	
	private static double getQuartil(List<Double> rilevations, int quartile) {

		if (rilevations.size() > 1) {
			Collections.sort(rilevations);
			float quartilPer = 0f;
			if (quartile == 1) {
				quartilPer = 0.25f;
			} else if (quartile == 2) {
				quartilPer = 0.50f;
			} else if (quartile == 3) {
				quartilPer = 0.75f;
			}

			// http://www.vias.org/tmdatanaleng/cc_quartile.html

			float position = Math.round(quartilPer * (rilevations.size()));

			int firstPos = new Long(Math.round(position)).intValue() - 1;

			float valueAtFirst = ((rilevations.get(firstPos)).floatValue());
			float valueAtSecond = ((rilevations.get(firstPos + 1)).floatValue());

			return valueAtFirst + (position - new Float(firstPos).floatValue())
					* (valueAtSecond - valueAtFirst);
		} else if (rilevations.size() == 1)
			return rilevations.get(0);
		else
			return 0d;
	}

}
