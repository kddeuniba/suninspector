package sumatra;

import java.util.Iterator;

import sumatra.model.Window;

 
public class WindowProducer implements Iterable<Window> {

	long startingTime;
	int windowsSize;
	String network;
	/**
	 * time in ms
	 */
	long snapshotTimeRange;
	
	private static final int M_TO_MS = 60000; 

	/**
	 * @param startingTime the starting time for the generation
	 * @param windowsSize the sze of the window as number of snapsho
	 * @param network the name of the network
	 * @param snapshotTimeRange the size of the snapshot in minutes
	 */
	public WindowProducer(long startingTime, int windowsSize,int snapshotTimeRange , String network) {
		this.startingTime = startingTime;
		this.windowsSize = windowsSize;
		this.network = network;
		this.snapshotTimeRange = snapshotTimeRange * M_TO_MS;
	}

	@Override
	public Iterator<Window> iterator() {
		return new WindowsIterator(this);
	}

	@Override
	public String toString() {
		return "WindowProducer [startingTime=" + startingTime
				+ ", windowsSize=" + windowsSize + ", network=" + network
				+ ", rangeSnapshot=" + snapshotTimeRange + "]";
	}
}
