# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table administrator (
  id                        bigint not null,
  firstname                 varchar(255),
  lastname                  varchar(255),
  username                  varchar(255),
  password                  varchar(255),
  email                     varchar(255),
  constraint pk_administrator primary key (id))
;

create table delivery_point (
  id                        bigint not null,
  owner                     bigint,
  street                    varchar(255),
  position                  varchar(255),
  active                    boolean,
  status                    varchar(9),
  constraint ck_delivery_point_status check (status in ('simulated','stopped','running')),
  constraint pk_delivery_point primary key (id))
;

create table owner (
  id                        bigint not null,
  firstname                 varchar(255),
  lastname                  varchar(255),
  username                  varchar(255),
  password                  varchar(255),
  email                     varchar(255),
  company                   varchar(255),
  constraint pk_owner primary key (id))
;

create table pvplant (
  id                        bigint not null,
  peak_power                float,
  area                      float,
  type                      varchar(14),
  place                     varchar(8),
  technology                varchar(15),
  angle                     float,
  aspect_angle              float,
  deliveryPoint             bigint,
  constraint ck_pvplant_type check (type in ('stand-alone','grid-connected')),
  constraint ck_pvplant_place check (place in ('building','free')),
  constraint ck_pvplant_technology check (technology in ('CdTe','CIGS','amorphous','Ibride','polycrystalline','nanocristalline','CIS','monocristalline')),
  constraint pk_pvplant primary key (id))
;

create table production (
  pv_plant_id               bigint not null,
  timestamp                 timestamp not null,
  energy                    float,
  temperature               float)
;

create table trend_cluster (
  id                        bigint not null,
  start_time                timestamp,
  end_time                  timestamp,
  polyline                  text,
  pvplant_ids               text,
  constraint pk_trend_cluster primary key (id))
;

create sequence administrator_seq;

create sequence delivery_point_seq;

create sequence owner_seq;

create sequence pvplant_seq;

create sequence production_seq;

create sequence trend_cluster_seq;

alter table delivery_point add constraint fk_delivery_point_owner_1 foreign key (owner) references owner (id);
create index ix_delivery_point_owner_1 on delivery_point (owner);
alter table pvplant add constraint fk_pvplant_deliveryPoint_2 foreign key (deliveryPoint) references delivery_point (id);
create index ix_pvplant_deliveryPoint_2 on pvplant (deliveryPoint);



# --- !Downs

drop table if exists administrator cascade;

drop table if exists delivery_point cascade;

drop table if exists owner cascade;

drop table if exists pvplant cascade;

drop table if exists production cascade;

drop table if exists trend_cluster cascade;

drop sequence if exists administrator_seq;

drop sequence if exists delivery_point_seq;

drop sequence if exists owner_seq;

drop sequence if exists pvplant_seq;

drop sequence if exists production_seq;

drop sequence if exists trend_cluster_seq;

