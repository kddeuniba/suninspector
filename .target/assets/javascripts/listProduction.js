var selectedId = -1;

require([ "datatables/jquery.dataTables" ], function(dt) {
		
	jsRoutes.controllers.DeliveryPoint.list().ajax({
	    success: function(data) {
	    	loadDataInTable(data);
	      },
	      error: function() {
	        alert("Error!")
	      }
	    });
	
	
	function loadDataInTable(data) {
		
		var oTable = $('#dpTable').dataTable({
			 "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
			 "bPaginate": true,
		     "bLengthChange": true,
		     "bFilter": true,
		     "bSort": true,
		     "bInfo": true,
		     "bAutoWidth": true,
		     "bDestroy": true,
		     "aaData": data,
		     "aoColumns": [
		        { "mDataProp": "idString" },
		        { "mDataProp": "street" },
		        { "mDataProp": "pvPlant.peakPower" },
		        { "mDataProp": "pvPlant.area" },
		        { "mDataProp": "pvPlant.type" },
		        { "mDataProp": "pvPlant.place" },
		        { "mDataProp": "pvPlant.technology" },
		        { "mDataProp": "status" },
		        { "mDataProp": "owner" }
		    ]
		});	
		
		
		$("#dpTable tbody").click(function(event) {
			$(oTable.fnSettings().aoData).each(function (){
				$(this.nTr).removeClass('row_selected');
			});
			$(event.target.parentNode).addClass('row_selected');
			selectedId = $(event.target.parentNode.childNodes[0]).text();
			loadLastProduction(selectedId);
		});
	}
	
	function loadLastProduction(id){
		
		jsRoutes.controllers.Production.list(id).ajax({
		    success: function(data) {
		    	loadProductionInTable(data);
		      },
		      error: function() {
		        alert("Error!")
		      }
		    });
	}
	
	function loadProductionInTable(data){
		
		var oTable = $('#prodTable').dataTable({
			 "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
			 "bPaginate": true,
		     "bLengthChange": true,
		     "bFilter": true,
		     "bSort": true,
		     "bInfo": true,
		     "bAutoWidth": true,
		     "bDestroy": true,
		     "aaData": data,
		     "aoColumns": [
		        { "mDataProp": "pvPlantId" },
		        { "mDataProp": "timestamp" },
		        { "mDataProp": "energy" },
		        { "mDataProp": "temperature" }
		    ]
		});	
	}
	
});
