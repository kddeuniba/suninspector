var selectedId = -1;

require([ "datatables/jquery.dataTables" ], function(dt) {
		
	jsRoutes.controllers.DeliveryPoint.list().ajax({
	    success: function(data) {
	    	loadDataInTable(data);
	      },
	      error: function() {
	        alert("Error!")
	      }
	    });
	
	
	function loadDataInTable(data) {
		
		var oTable = $('#dpTable').dataTable({
			 "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
			 "bPaginate": true,
		     "bLengthChange": true,
		     "bFilter": true,
		     "bSort": true,
		     "bInfo": true,
		     "bAutoWidth": true,
		     "iDisplayLength": 5,
		     "aaData": data,
		     "aoColumns": [
		        { "mDataProp": "idString" },
		        { "mDataProp": "street" },
		        { "mDataProp": "pvPlant.peakPower" },
		        { "mDataProp": "pvPlant.area" },
		        { "mDataProp": "pvPlant.type" },
		        { "mDataProp": "pvPlant.place" },
		        { "mDataProp": "pvPlant.technology" },
		        { "mDataProp": "status" },
		        { "mDataProp": "owner" }
		    ]
		});	
		
		
		$("#dpTable tbody").click(function(event) {
			$(oTable.fnSettings().aoData).each(function (){
				$(this.nTr).removeClass('row_selected');
			});
			$(event.target.parentNode).addClass('row_selected');
			selectedId = $(event.target.parentNode.childNodes[0]).text();
			var status = $(event.target.parentNode.childNodes[7]).text();
			
			//enable buttons
			enableButtons(status);
		});
	}
	
	function enableButtons(status){
		
		switch (status) {
		case "simulated":
			$('#stop').removeClass('disabled');
			$("#stop").attr("href", "/datagenerator/"+ selectedId +"/stop")
			$('#reset').removeClass('disabled');
			$("#reset").attr("href", "/datagenerator/"+ selectedId +"/reset")
			break;
			
		case "stopped":
			$('#start').removeClass('disabled');
			$("#start").attr("href", "/datagenerator/"+ selectedId +"/start")
			break;

		default:
			break;
		}
	}
	
});
