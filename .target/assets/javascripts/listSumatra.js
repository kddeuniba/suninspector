var selectedId = -1;

require([ "datatables/jquery.dataTables" ], function(dt) {
		
	jsRoutes.controllers.Sumatra.listJson().ajax({
	    success: function(data) {
	    	loadDataInTable(data);
	      },
	      error: function() {
	        alert("Error!")
	      }
	    });
	
	
	function loadDataInTable(data) {
		
		var oTable = $('#smTable').dataTable({
			 "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
			 "bPaginate": true,
		     "bLengthChange": true,
		     "bFilter": true,
		     "bSort": true,
		     "bInfo": true,
		     "bAutoWidth": true,
		     "bDestroy": true,
		     "aaData": data,
		     "aoColumns": [
		        { "mDataProp": "id" },
		        { "mDataProp": "network" },
		        { "mDataProp": "startingTime" },
		        { "mDataProp": "intervalMinute" },
		        { "mDataProp": "windowSize" },
		        { "mDataProp": "threshold" },
		        { "mDataProp": "maxDistanceEdge" }
		    ]
		});	
		
		
		$("#smTable tbody").click(function(event) {
			$(oTable.fnSettings().aoData).each(function (){
				$(this.nTr).removeClass('row_selected');
			});
			$(event.target.parentNode).addClass('row_selected');
			selectedId = $(event.target.parentNode.childNodes[0]).text();
			loadLastProduction(selectedId);
		});
	}
	
	function loadLastProduction(id){
		
		jsRoutes.controllers.Production.list(id).ajax({
		    success: function(data) {
		    	loadProductionInTable(data);
		      },
		      error: function() {
		        alert("Error!")
		      }
		    });
	}
	
});
