import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "suninspector"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    javaCore,
    javaJdbc,
    javaEbean,
    "org.postgresql" % "postgresql" % "9.2-1002-jdbc4",
    "org.webjars" % "webjars-play" % "2.1.0-1",
    "com.fasterxml.jackson.dataformat" % "jackson-dataformat-csv" % "2.2.2",
    "org.webjars" % "requirejs" % "2.1.5"
  )

  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here      
  )

}
